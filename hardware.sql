CREATE DATABASE CHEST SET utf8 COLLATE utf8mb4_polish_ci;

CREATE TABLE HARDWARE (
    id  char AUTO_INCREMENT NOT NULL PRIMARY KEY,
    eqName nvarchar(32) NOT NULL,

    target nvarchar(128) NOT NULL,
    repairs int FOREIGN KEY REFERENCES REPAIR(id),
    equipment int FOREIGN KEY REFERENCES EQUIPMENT(id),
    officeID int FOREIGN KEY REFERENCES OFFICE(id) NOT NULL,
    createdAt datetime2 NOT NULL,
    updatedAt datetime2 NOT NULL
    networkID int FOREIGN KEY REFERENCES NETWORK(id) NOT NULL, 

);



CREATE TABLE NETWORK (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    networkName char(32) NOT NULL,
    ipAdress char(16) NOT NULL, --IP adress v4
    mask char(16) NOT NULL, --IP adress v4
    gateway char(16) NOT NULL,  --IP adress v4
    DNS1 char(16), --IP adress v4
    DNS2 char(16), --IP adress v4
    floor char(2)

)


CREATE TABLE LOCATION (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    country nvarchar(96) NOT NULL,
    town nvarchar(128) NOT NULL,
    street nvarchar(128) NOT NULL,
    streetNr nvarchar(4) NOT NULL,
    houseNr nvarchar(4) NOT NULL,
    postcode nvarchar(64) NOT NULL,
    alias nvarchar(128) --for special point whence messenger send or who is the receiver
)


CREATE TABLE EQUIPMENT (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    communication nvarchar(128), --model
    pamiec nvarchar(128), --model
    processor nvarchar(128), --model
    mainBoard nvarchar(128), --model
    ports  FOREIGN KEY REFERENCES PORTS(id),
    insurenceID FOREIGN KEY REFERENCES INSURENCE(id),
    macAdress char(17),
    ipAdress char(16) NOT NULL --IP adress v4
)


CREATE TABLE MEMORY (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    memoryType nvarchar(128) NOT NULL,
    memoryName nvarchar(128) NOT NULL,
    model nvarchar(128) NOT NULL,
    specificationID FOREIGN KEY REFERENCES SPECIFICATION(id),
    size nvarchar(15)
)


CREATE TABLE REPAIR (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    repairman nvarchar(128) DEFAULT 'Serwis Point', --name -> business / user or serwis point
    repairEntry nvarchar(128),
    prownessAfter char(1), --to know repair serviceableness 
    messengerID int FOREIGN KEY REFERENCES MESSENGER(id)
)


CREATE TABLE MESSENGER (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    business nvarchar (64) NOT NULL,
    sender int FOREIGN KEY REFERENCES PERSONAL(id),
    receiver int FOREIGN KEY REFERENCES PERSONAL(id),
    sendDate datetime2 NOT NULL, --some getdate() ...
    receiveDate datetime2 NOT NULL, --some getdate() ...
    trackNr char(256),
    payment nvarchar(256),
    reception int FOREIGN KEY REFERENCES LOCATION(id)
)


CREATE TABLE USER (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    username nvarchar(32) NOT NULL,
    passwordHash nvarchar(256)  NOT NULL,--hashed
    userRights nvarchar(256) NOT NULL,
    groups nvarchar(256),
    holiday int FOREIGN KEY REFERENCES HOLIDAY(id), --switch to null if holidays end
    office int FOREIGN KEY REFERENCES OFFICE(officeNr) NOT NULL,
    workplace char(3), -- the place in office
    agreement int FOREIGN KEY REFERENCES AGREEMENT(id) NOT NULL,
    personal int FOREIGN KEY REFERENCES PERSONAL(id) NOT NULL,
    hardware int FOREIGN KEY  REFERENCESHARDWARE(id) NOT NULL,
    createdat datetime2 NOT NULL,
    createdat datetime2 NOT NULL
)


CREATE TABLE AGREEMENT (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    agreementType nvarchar(256),
    sigingDate datetime2 NOT NULL,
    endDate datetime2 NOT NULL,
    renunciation datetime2, -- time to end job
    bruttoPayment char(20), --in PLN
    nettoPayment char(20), --in PLN
    location int FOREIGN KEY REFERENCES LOCATION(id),
    paymentDay datetime --some function setting string 12 with name of the day
    workerCaption boolean NOT NULL,
    supCaption boolean NOT NULL,
    workingHours nvarchar(128) NOT NULL, --etc. 8:00 am. - 8:00 pm.
    monthHours char(3) NOT NULL
)


CREATE TABLE PERSONAL (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    forename nvarchar(64) NOT NULL, --name is forbidden column
    surname nvarchar(64) NOT NULL,
    pesel char(11),
    location char FOREIGN KEY REFERENCES LOCATION(id)
)


CREATE TABLE AUCTION (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    auctionNr char(10) NOT NULL,
    auctionType nvarchar(128) DEFAULT 'Buy Now',
    sellerProfile nvarchar(512) ,
    consumerProfile nvarchar(512) ,
    messengerid int FOREIGN KEY REFERENCES MESSENGER(id), 
    dealNr char(100) NOT NULL,
    auctionsPage nvarchar(512)
)


CREATE TABLE OFFICE (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    area nvarchar(10), --m^2
    officeNr nvarchar(3) NOT NULL,
    floor char(2)
)


CREATE TABLE HOLIDAY (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    holidayType nvarchar(128) NOT NULL,
    holidayStart datetime2,
    holidayEnd datetime2,
    location FOREIGN KEY REFERENCES LOCATION(id),
    isPayment boolean DEFAULT 1,
    createdAt datetime2 NOT NULL,
    updatedAt datetime2 NOT NULL
)

CREATE TABLE SPECIFICATION (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    serialNr nvarchar(128) NOT NULL,
    catologueNr nvarchar(128), 
    model nvarchar(64) NOT NULL,
    business nvarchar (64) NOT NULL,
    prowess char(1) NOT NULL, --from 1 to 5
    engaged boolean NOT NULL DEFAULT 1,
    productionYear timestamp,
    obtainPoint int FOREIGN KEY REFERENCES AUCTION(id) NOT NULL
)

CREATE TABLE PORTS (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
     cdROM nvarchar(128), --model
    hdmi char(1),
    vga char(1),
    audioIn char(1),
    audioOut char(1),
    sdReader char(2),
    usb char(3),
    ethernetPorts char(2), --RJ45
    phoneSockets char(2), --RJ11
)

CREATE TABLE INSURENCE (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    insurenceName varchar(64) NOT NULL,
    sort varchar(96) NOT NULL,
    startDate datetime2 NOT NULL,
    LengthOf timestamp NOT NULL,
    warranty timestamp NOT NULL,
    rangeOf varchar(32) --country / state / town

)


--if nvarchar doesn't work - use varchar
--no engine due MySQL issues
--forbidden names: https://dev.mysql.com/doc/refman/8.0/en/keywords.html

--DROP DATABASE CHEST
--DROP TABLE hardware
--DROP TABLE NETWORK
--DROP TABLE LOCATION
--DROP TABLE EQUIPMENT
--DROP TABLE MEMORY
--DROP TABLE REPAIR
--DROP TABLE MESSENGER
--DROP TABLE USER
--DROP TABLE AGREEMENT
--DROP TABLE PERSONAL
--DROP TABLE AUCTION
--DROP TABLE OFFICE
--DROP TABLE HOLIDAY
--DROP TABLE SPECIFICATION
--DROP TABLE PORTS